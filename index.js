'use strict';
const
  BootBot = require('bootbot'),
  config = require('config'),
  request = require('request'),
  express = require('express'),
  body_parser = require('body-parser'),
  echoModule = require('./modules/echo');
// var MongoClient = require('mongodb').MongoClient;
//
//
// var uri = "mongodb+srv://legendarydev:06010059Porto@smmlegendary-npuz6.mongodb.net/test"
//
// MongoClient.connect(uri, function(err, client) {
//    const collection = client.db("test").collection("devices");
//    // perform actions on the collection object
//    client.close();
// });

const bot = new BootBot({
  accessToken: config.get('accessToken'),
  verifyToken: config.get('verifyToken'),
  appSecret: config.get('appSecret')
});

bot.hear(['hi', 'hello', 'hey'], (payload, chat) => {
chat.say('Oie');
});


let usersMessage = [];
let conversation = [];

bot.on('message', (payload, chat) => {

  let text = payload.message.text;
  const userId = payload.sender.id;

  chat.getUserProfile().then((user) => {
    chat.say(`Hello, ${user.first_name}!`);
  });

  // console.log("message sent: " + text);
  console.log("User ID: " + userId);

  usersMessage.push(text);
//  console.log(usersMessage);

  conversation.id = {
    id : userId,
    messages : usersMessage
  };

  console.log(conversation);
});



bot.start(config.get('botPort'));
